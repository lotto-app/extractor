// @flow

import * as config from "./config";
import { createExtraction, shouldCreateExtraction } from "./utils";
import ExtractionModel from "./models";

let SHOULD_EXTRACT = false;

async function getLastExtraction() {
  const extractions = await ExtractionModel.find({})
    .sort({ $natural: -1 })
    .limit(1);
  let extraction = null;

  if (extractions.length > 0) {
    [extraction] = extractions;
  }

  return extraction;
}

async function storeExtraction() {
  const extraction = createExtraction();
  const extractionModel = new ExtractionModel({ ...extraction });

  return extractionModel.save();
}

async function extractNumbers() {
  console.log("About to extract...");

  if (SHOULD_EXTRACT) {
    await storeExtraction();
  } else {
    const extraction = await getLastExtraction();

    if (extraction) {
      if (shouldCreateExtraction(extraction.date)) {
        SHOULD_EXTRACT = true;
        await storeExtraction();
      }
    } else {
      SHOULD_EXTRACT = true;
      await storeExtraction();
    }
  }

  setTimeout(() => {
    extractNumbers();
  }, config.EXTRACTION_INTERVAL);
}

extractNumbers();
