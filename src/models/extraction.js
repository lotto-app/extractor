import mongoose from "mongoose";
import moment from "moment";

const { Schema } = mongoose;
mongoose.Promise = Promise;

const Extraction = new Schema({
  numbers: { type: Array },
  numbersHash: { type: String },
  date: {
    type: Date,

    default: () => {
      return moment().toDate();
    }
  }
});

const ExtractionModel = mongoose.model("Extraction", Extraction);

export default ExtractionModel;
