import "dotenv";

// extraction time in s
let extractionIntervalTemp = process.env.EXTRACTION_INTERVAL || 1;
extractionIntervalTemp *= 1000;

let mongoUriTemp = "mongodb://localhost:27017/kubernetes-lotto";

if (
  process.env.MONGO_IP &&
  process.env.MONGO_USER &&
  process.env.MONGO_PASSWORD
) {
  mongoUriTemp = `mongodb://${process.env.MONGO_USER}:${process.env.MONGO_PASSWORD}@${process.env.MONGO_IP}:27017/kubernetes-lotto?authSource=admin`;
}

const MONGO_URI = mongoUriTemp;
const EXTRACTION_INTERVAL = extractionIntervalTemp;

export { EXTRACTION_INTERVAL, MONGO_URI };
