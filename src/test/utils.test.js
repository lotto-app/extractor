import { createExtraction, generateExtractionHash, shouldCreateExtraction } from "../utils";

test("a generated extraction hash", () => {
  const numbersArray = [1, 2, 3, 4, 5, 6];
  const numbersArrayHash = "123456";
  const wrongNumbersArrayHash = "1234567";

  const generatedHash = generateExtractionHash(numbersArray);

  expect(generatedHash).toBe(numbersArrayHash);
  expect(generatedHash).not.toBe(wrongNumbersArrayHash);
});

test("an extraction", () => {
  const extraction = createExtraction();

  expect(typeof extraction).toBe("object");
  expect("numbers" in extraction).toBeTruthy();
  expect(Array.isArray(extraction.numbers)).toBeTruthy();
  expect(extraction.numbers.length).toBeGreaterThan(0);
  expect("numbersHash" in extraction).toBeTruthy();
  expect(typeof extraction.numbersHash).toBe("string");
  expect(extraction.numbersHash.length).toBeGreaterThan(0);
  expect(extraction.numbersHash).toBe(
    generateExtractionHash(extraction.numbers)
  );
});

test("wether an extraction should be done based on a timestamp", () => {
  const strDate1 = "2019-09-25T13:20:56.590Z";
  const strDate2 = "2050-09-25T13:20:56.590Z";

  expect(shouldCreateExtraction(strDate1)).toBeTruthy();
  expect(shouldCreateExtraction(strDate2)).not.toBeTruthy();
});
