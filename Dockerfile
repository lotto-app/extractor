# --- BASE ---
FROM node:latest

WORKDIR /usr/src/extractor

# Copy files to working directory
COPY .npmrc .
COPY yarn.lock .
COPY package.json .

# Install only dependencies for production
RUN yarn install

COPY . .

# Start the service
CMD [ "yarn", "start" ]
