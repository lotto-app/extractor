// @flow

import moment from "moment";

import * as config from "./config";

function shouldCreateExtraction(strDate: String) {
  const mDate1 = moment(strDate);
  const mDate2 = moment();

  const dateDiff = mDate2.diff(mDate1, "ms");

  return dateDiff >= config.EXTRACTION_INTERVAL;
}

function generateExtractionHash(numbers: Array) {
  const tempNumbers = [...numbers];
  tempNumbers.sort();

  return tempNumbers.join("");
}

function createExtraction() {
  const extractionValues = [];
  const extractedNumbers = 6;
  const numbers = [];

  for (let i = 0; i < 49; i += 1) {
    extractionValues.push(i + 1);
  }

  for (let i = 0; i < extractedNumbers; i += 1) {
    const randomIndex = Math.floor(Math.random() * extractionValues.length);

    numbers.push(extractionValues[randomIndex]);
    extractionValues.splice(randomIndex, 1);
  }

  const numbersHash = generateExtractionHash(numbers);

  return {
    numbers,
    numbersHash
  };
}

export { createExtraction, generateExtractionHash, shouldCreateExtraction };
