import mongoose from "mongoose";

import * as config from "../config";

import ExtractionModel from "./extraction";

mongoose
  .connect(config.MONGO_URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  })
  .then(() => {
    console.log("MongoDB connection established.");
  })
  .catch(() => {
    console.error("There was an error while connecting to MongoDB");

    return process.exit(1);
  });

export default ExtractionModel;
